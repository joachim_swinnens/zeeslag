﻿namespace Zeeslag
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnKlaar = new System.Windows.Forms.Button();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.btnSchiet = new System.Windows.Forms.Button();
            this.txtLegende = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cboVKolom = new System.Windows.Forms.ComboBox();
            this.cboVRij = new System.Windows.Forms.ComboBox();
            this.cboVOrient = new System.Windows.Forms.ComboBox();
            this.btnVPlaats = new System.Windows.Forms.Button();
            this.cboVSchip = new System.Windows.Forms.ComboBox();
            this.txtTestVijand = new System.Windows.Forms.TextBox();
            this.rtxtZeeSpeler = new System.Windows.Forms.RichTextBox();
            this.rtxtZeeComp = new System.Windows.Forms.RichTextBox();
            this.cboSRij = new System.Windows.Forms.ComboBox();
            this.cboSKolom = new System.Windows.Forms.ComboBox();
            this.btnRestart = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnOpslaan = new System.Windows.Forms.Button();
            this.btnLaden = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnKlaar
            // 
            this.btnKlaar.Location = new System.Drawing.Point(1066, 109);
            this.btnKlaar.Name = "btnKlaar";
            this.btnKlaar.Size = new System.Drawing.Size(117, 38);
            this.btnKlaar.TabIndex = 0;
            this.btnKlaar.Text = "Klaar";
            this.btnKlaar.UseVisualStyleBackColor = true;
            this.btnKlaar.Click += new System.EventHandler(this.btnKlaar_Click);
            // 
            // txtInfo
            // 
            this.txtInfo.Location = new System.Drawing.Point(39, 42);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.ReadOnly = true;
            this.txtInfo.Size = new System.Drawing.Size(245, 82);
            this.txtInfo.TabIndex = 1;
            // 
            // btnSchiet
            // 
            this.btnSchiet.Location = new System.Drawing.Point(600, 256);
            this.btnSchiet.Name = "btnSchiet";
            this.btnSchiet.Size = new System.Drawing.Size(171, 77);
            this.btnSchiet.TabIndex = 4;
            this.btnSchiet.Text = "Schiet!";
            this.btnSchiet.UseVisualStyleBackColor = true;
            this.btnSchiet.Click += new System.EventHandler(this.btnSchiet_Click);
            // 
            // txtLegende
            // 
            this.txtLegende.Location = new System.Drawing.Point(520, 375);
            this.txtLegende.Multiline = true;
            this.txtLegende.Name = "txtLegende";
            this.txtLegende.ReadOnly = true;
            this.txtLegende.Size = new System.Drawing.Size(251, 188);
            this.txtLegende.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(962, 177);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 19);
            this.label1.TabIndex = 6;
            this.label1.Text = "Dit is jou zee:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(116, 183);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 19);
            this.label2.TabIndex = 7;
            this.label2.Text = "Zee van de vijand:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(683, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Vliegdekschip (5)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(777, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Slagschip (4)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(861, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Kruiser (3)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(683, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Onderzeeër (3)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(777, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Mijneveger (2)";
            // 
            // cboVKolom
            // 
            this.cboVKolom.FormattingEnabled = true;
            this.cboVKolom.Location = new System.Drawing.Point(996, 58);
            this.cboVKolom.Name = "cboVKolom";
            this.cboVKolom.Size = new System.Drawing.Size(46, 21);
            this.cboVKolom.TabIndex = 13;
            // 
            // cboVRij
            // 
            this.cboVRij.FormattingEnabled = true;
            this.cboVRij.Location = new System.Drawing.Point(943, 58);
            this.cboVRij.Name = "cboVRij";
            this.cboVRij.Size = new System.Drawing.Size(46, 21);
            this.cboVRij.TabIndex = 14;
            // 
            // cboVOrient
            // 
            this.cboVOrient.FormattingEnabled = true;
            this.cboVOrient.Location = new System.Drawing.Point(1048, 56);
            this.cboVOrient.Name = "cboVOrient";
            this.cboVOrient.Size = new System.Drawing.Size(104, 21);
            this.cboVOrient.TabIndex = 15;
            // 
            // btnVPlaats
            // 
            this.btnVPlaats.Location = new System.Drawing.Point(1158, 56);
            this.btnVPlaats.Name = "btnVPlaats";
            this.btnVPlaats.Size = new System.Drawing.Size(86, 22);
            this.btnVPlaats.TabIndex = 16;
            this.btnVPlaats.Text = "Plaats schip";
            this.btnVPlaats.UseVisualStyleBackColor = true;
            this.btnVPlaats.Click += new System.EventHandler(this.btnVPlaats_Click);
            // 
            // cboVSchip
            // 
            this.cboVSchip.FormattingEnabled = true;
            this.cboVSchip.Location = new System.Drawing.Point(996, 17);
            this.cboVSchip.Name = "cboVSchip";
            this.cboVSchip.Size = new System.Drawing.Size(162, 21);
            this.cboVSchip.TabIndex = 17;
            // 
            // txtTestVijand
            // 
            this.txtTestVijand.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTestVijand.Location = new System.Drawing.Point(343, 17);
            this.txtTestVijand.Multiline = true;
            this.txtTestVijand.Name = "txtTestVijand";
            this.txtTestVijand.ReadOnly = true;
            this.txtTestVijand.Size = new System.Drawing.Size(299, 179);
            this.txtTestVijand.TabIndex = 18;
            // 
            // rtxtZeeSpeler
            // 
            this.rtxtZeeSpeler.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtZeeSpeler.Location = new System.Drawing.Point(844, 227);
            this.rtxtZeeSpeler.Name = "rtxtZeeSpeler";
            this.rtxtZeeSpeler.ReadOnly = true;
            this.rtxtZeeSpeler.Size = new System.Drawing.Size(370, 290);
            this.rtxtZeeSpeler.TabIndex = 19;
            this.rtxtZeeSpeler.Text = "";
            // 
            // rtxtZeeComp
            // 
            this.rtxtZeeComp.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtZeeComp.Location = new System.Drawing.Point(39, 227);
            this.rtxtZeeComp.Name = "rtxtZeeComp";
            this.rtxtZeeComp.ReadOnly = true;
            this.rtxtZeeComp.Size = new System.Drawing.Size(393, 290);
            this.rtxtZeeComp.TabIndex = 20;
            this.rtxtZeeComp.Text = "";
            // 
            // cboSRij
            // 
            this.cboSRij.FormattingEnabled = true;
            this.cboSRij.Location = new System.Drawing.Point(497, 270);
            this.cboSRij.Name = "cboSRij";
            this.cboSRij.Size = new System.Drawing.Size(68, 21);
            this.cboSRij.TabIndex = 21;
            // 
            // cboSKolom
            // 
            this.cboSKolom.FormattingEnabled = true;
            this.cboSKolom.Location = new System.Drawing.Point(497, 312);
            this.cboSKolom.Name = "cboSKolom";
            this.cboSKolom.Size = new System.Drawing.Size(68, 21);
            this.cboSKolom.TabIndex = 22;
            // 
            // btnRestart
            // 
            this.btnRestart.Location = new System.Drawing.Point(242, 598);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(95, 39);
            this.btnRestart.TabIndex = 24;
            this.btnRestart.Text = "Nieuw Spel";
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(343, 598);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(95, 39);
            this.btnExit.TabIndex = 25;
            this.btnExit.Text = "Sluiten";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnOpslaan
            // 
            this.btnOpslaan.Location = new System.Drawing.Point(40, 598);
            this.btnOpslaan.Name = "btnOpslaan";
            this.btnOpslaan.Size = new System.Drawing.Size(95, 39);
            this.btnOpslaan.TabIndex = 26;
            this.btnOpslaan.Text = "Spel Opslaan";
            this.btnOpslaan.UseVisualStyleBackColor = true;
            this.btnOpslaan.Click += new System.EventHandler(this.btnOpslaan_Click);
            // 
            // btnLaden
            // 
            this.btnLaden.Location = new System.Drawing.Point(141, 598);
            this.btnLaden.Name = "btnLaden";
            this.btnLaden.Size = new System.Drawing.Size(95, 39);
            this.btnLaden.TabIndex = 27;
            this.btnLaden.Text = "Spel Laden";
            this.btnLaden.UseVisualStyleBackColor = true;
            this.btnLaden.Click += new System.EventHandler(this.btnLaden_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1285, 649);
            this.Controls.Add(this.btnLaden);
            this.Controls.Add(this.btnOpslaan);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnRestart);
            this.Controls.Add(this.cboSKolom);
            this.Controls.Add(this.cboSRij);
            this.Controls.Add(this.rtxtZeeComp);
            this.Controls.Add(this.rtxtZeeSpeler);
            this.Controls.Add(this.txtTestVijand);
            this.Controls.Add(this.cboVSchip);
            this.Controls.Add(this.btnVPlaats);
            this.Controls.Add(this.cboVOrient);
            this.Controls.Add(this.cboVRij);
            this.Controls.Add(this.cboVKolom);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtLegende);
            this.Controls.Add(this.btnSchiet);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.btnKlaar);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnKlaar;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.Button btnSchiet;
        private System.Windows.Forms.TextBox txtLegende;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboVKolom;
        private System.Windows.Forms.ComboBox cboVRij;
        private System.Windows.Forms.ComboBox cboVOrient;
        private System.Windows.Forms.Button btnVPlaats;
        private System.Windows.Forms.ComboBox cboVSchip;
        private System.Windows.Forms.TextBox txtTestVijand;
        private System.Windows.Forms.RichTextBox rtxtZeeSpeler;
        private System.Windows.Forms.RichTextBox rtxtZeeComp;
        private System.Windows.Forms.ComboBox cboSRij;
        private System.Windows.Forms.ComboBox cboSKolom;
        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnOpslaan;
        private System.Windows.Forms.Button btnLaden;
    }
}

