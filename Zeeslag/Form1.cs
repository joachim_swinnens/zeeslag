﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zeeslag
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string[,] arrZeePlayer = new string[,] {{ "-","A", "B", "C", "D", "E", "F", "G", "H", "I", "J" },
                                             {"1","~","~","~","~","~","~","~","~","~","~"},
                                             {"2","~","~","~","~","~","~","~","~","~","~"},
                                             {"3","~","~","~","~","~","~","~","~","~","~"},
                                             {"4","~","~","~","~","~","~","~","~","~","~"},
                                             {"5","~","~","~","~","~","~","~","~","~","~"},
                                             {"6","~","~","~","~","~","~","~","~","~","~"},
                                             {"7","~","~","~","~","~","~","~","~","~","~"},
                                             {"8","~","~","~","~","~","~","~","~","~","~"},
                                             {"9","~","~","~","~","~","~","~","~","~","~"},
                                             {"10","~","~","~","~","~","~","~","~","~","~"}};
        string[,] arrZeeComputer = new string[,] {{ "-","A", "B", "C", "D", "E", "F", "G", "H", "I", "J" },
                                             {"1","~","~","~","~","~","~","~","~","~","~"},
                                             {"2","~","~","~","~","~","~","~","~","~","~"},
                                             {"3","~","~","~","~","~","~","~","~","~","~"},
                                             {"4","~","~","~","~","~","~","~","~","~","~"},
                                             {"5","~","~","~","~","~","~","~","~","~","~"},
                                             {"6","~","~","~","~","~","~","~","~","~","~"},
                                             {"7","~","~","~","~","~","~","~","~","~","~"},
                                             {"8","~","~","~","~","~","~","~","~","~","~"},
                                             {"9","~","~","~","~","~","~","~","~","~","~"},
                                             {"10","~","~","~","~","~","~","~","~","~","~"}};
        string[,] arrOpstellingComputer = new string[,] {{ "-","A", "B", "C", "D", "E", "F", "G", "H", "I", "J" },
                                             {"1","~","~","~","~","~","~","~","~","~","~"},
                                             {"2","~","~","~","~","~","~","~","~","~","~"},
                                             {"3","~","~","~","~","~","~","~","~","~","~"},
                                             {"4","~","~","~","~","~","~","~","~","~","~"},
                                             {"5","~","~","~","~","~","~","~","~","~","~"},
                                             {"6","~","~","~","~","~","~","~","~","~","~"},
                                             {"7","~","~","~","~","~","~","~","~","~","~"},
                                             {"8","~","~","~","~","~","~","~","~","~","~"},
                                             {"9","~","~","~","~","~","~","~","~","~","~"},
                                             {"10","~","~","~","~","~","~","~","~","~","~"}};

        string[] rijen = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
        string[] kolommen = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };
        string[] orientatie = { "Horizontaal", "Vertikaal" };
        string[] schepen = { "Vliegdekschip", "Slagschip", "Kruiser", "Onderzeeër", "Mijneveger" };

        int spelerGeraakt = 0;
        int compGeraakt = 0;
        int beurten = 0;
        //public void Setup()
        //{
        //    string[,] arrZeePlayer = new string[,] {{ "-","A", "B", "C", "D", "E", "F", "G", "H", "I", "J" },
        //                                     {"1","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"2","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"3","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"4","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"5","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"6","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"7","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"8","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"9","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"10","~","~","~","~","~","~","~","~","~","~"}};
        //    string[,] arrZeeComputer = new string[,] {{ "-","A", "B", "C", "D", "E", "F", "G", "H", "I", "J" },
        //                                     {"1","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"2","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"3","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"4","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"5","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"6","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"7","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"8","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"9","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"10","~","~","~","~","~","~","~","~","~","~"}};
        //    string[,] arrOpstellingComputer = new string[,] {{ "-","A", "B", "C", "D", "E", "F", "G", "H", "I", "J" },
        //                                     {"1","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"2","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"3","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"4","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"5","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"6","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"7","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"8","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"9","~","~","~","~","~","~","~","~","~","~"},
        //                                     {"10","~","~","~","~","~","~","~","~","~","~"}};

        //    string[] rijen = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
        //    string[] kolommen = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };
        //    string[] orientatie = { "Horizontaal", "Vertikaal" };
        //    string[] schepen = { "Vliegdekschip", "Slagschip", "Kruiser", "Onderzeeër", "Mijneveger" };

        //    int spelerGeraakt = 0;
        //    int compGeraakt = 0;
        //    int beurten = 0;

        //}
        private void Form1_Load(object sender, EventArgs e)
        {
            txtLegende.Text = "Legende:" + Environment.NewLine +
                              "~ = Zee" + Environment.NewLine +
                              "N = Naast geschoten" + Environment.NewLine +
                              "G = Geraakt" + Environment.NewLine + Environment.NewLine +
                              "Schepen:" + Environment.NewLine +
                              "V = Vliegdekschip (5 lang)" + Environment.NewLine +
                              "S = Slagschip (4 lang)" + Environment.NewLine +
                              "K = Kruiser (3 lang)" + Environment.NewLine +
                              "O = Onderzeeër (3 lang)" + Environment.NewLine +
                              "M = Mijneveger (2 lang)";


            ToonZeePlayer();
            ToonZeeComp();

            ToonInfo();

            for (int i = 0; i < rijen.Length; i++)
            {
                cboVRij.Items.Add(rijen[i]);
            }
            for (int i = 0; i < kolommen.Length; i++)
            {
                cboVKolom.Items.Add(kolommen[i]);
            }
            for (int i = 0; i < orientatie.Length; i++)
            {
                cboVOrient.Items.Add(orientatie[i]);
            }
            for (int i = 0; i < schepen.Length; i++)
            {
                cboVSchip.Items.Add(schepen[i]);
            }

            for (int i = 0; i < rijen.Length; i++)
            {
                cboSRij.Items.Add(rijen[i]);
            }
            for (int i = 0; i < kolommen.Length; i++)
            {
                cboSKolom.Items.Add(kolommen[i]);
            }
            btnSchiet.Hide();
            btnOpslaan.Hide();
            cboSRij.Hide();
            cboSKolom.Hide();
            txtTestVijand.Hide();

        }

        private void ToonInfo()
        {

            txtInfo.Text = ("Welkom bij Zeeslag" + Environment.NewLine +
                                   $"Speler geraakt: {spelerGeraakt}" + Environment.NewLine +
                                   $"Tegenstander geraakt: {compGeraakt}" + Environment.NewLine +
                                   $"Aantal Beurten: {beurten}");
        }
        private void ToonZeePlayer()
        {
            rtxtZeeSpeler.Clear();
            string myString = "";
            for (int rij = 0; rij <= arrZeePlayer.GetUpperBound(0); rij++)
            {
                for (int kolom = 0; kolom <= arrZeePlayer.GetUpperBound(1); kolom++)
                {
                    myString = arrZeePlayer[rij, kolom].PadLeft(2) + " ";
                    if (arrZeePlayer[rij, kolom] == "~")
                    {
                        rtxtZeeSpeler.SelectionColor = Color.Blue;
                    }
                    else if ((rij == 0) || (kolom == 0))
                    {
                        rtxtZeeSpeler.SelectionColor = Color.DarkGray;
                    }
                    else if ((arrZeePlayer[rij, kolom] == "G") && (rij > 0))
                    {
                        rtxtZeeSpeler.SelectionColor = Color.Red;
                    }
                    else if (arrZeePlayer[rij, kolom] == "N")
                    {
                        rtxtZeeSpeler.SelectionColor = Color.Green;
                    }
                    else
                    {

                        rtxtZeeSpeler.SelectionColor = Color.Black;

                    }
                    rtxtZeeSpeler.AppendText(myString);
                }
                myString = "\n";
                rtxtZeeSpeler.SelectionColor = Color.Black;
                rtxtZeeSpeler.AppendText(myString);
            }
        }

        private void ToonZeeComp()
        {
            rtxtZeeComp.Clear();
            string myString = "";
            for (int rij = 0; rij <= arrZeeComputer.GetUpperBound(0); rij++)
            {
                for (int kolom = 0; kolom <= arrZeeComputer.GetUpperBound(1); kolom++)
                {
                    myString = arrZeeComputer[rij, kolom].PadLeft(2) + " ";
                    if (arrZeeComputer[rij, kolom] == "~")
                    {
                        rtxtZeeComp.SelectionColor = Color.Blue;
                    }
                    else if ((rij == 0) || (kolom == 0))
                    {
                        rtxtZeeComp.SelectionColor = Color.DarkGray;
                    }
                    else if ((arrZeeComputer[rij, kolom] == "G") && (rij > 0))
                    {
                        rtxtZeeComp.SelectionColor = Color.Red;
                    }
                    else if (arrZeeComputer[rij, kolom] == "N")
                    {
                        rtxtZeeComp.SelectionColor = Color.Green;
                    }
                    else
                    {

                        rtxtZeeComp.SelectionColor = Color.Black;

                    }
                    rtxtZeeComp.AppendText(myString);

                    //rtxtZeeComp.Text += arrZeeComputer[rij, kolom].PadLeft(2) + " ";
                }
                myString = "\n";
                rtxtZeeComp.SelectionColor = Color.Black;
                rtxtZeeComp.AppendText(myString);


            }
        }

        private void ToonTestVijand()
        {
            txtTestVijand.Clear();
            for (int rij = 0; rij <= arrOpstellingComputer.GetUpperBound(0); rij++)
            {
                for (int kolom = 0; kolom <= arrOpstellingComputer.GetUpperBound(1); kolom++)
                {
                    txtTestVijand.Text += arrOpstellingComputer[rij, kolom].PadLeft(2) + " ";
                }
                txtTestVijand.Text += Environment.NewLine;
            }
        }

        private void btnVPlaats_Click(object sender, EventArgs e)
        {



            int vRij = cboVRij.SelectedIndex + 1;
            int vKolom = cboVKolom.SelectedIndex + 1;
            int vOrien = cboVOrient.SelectedIndex + 1;
            int schipLengte = 0;
            string schipLetter = "";

            //parameters schip bepalen
            if (cboVSchip.SelectedIndex == 0)
            {
                schipLengte = 5;
                schipLetter = "V";
            }

            if (cboVSchip.SelectedIndex == 1)
            {
                schipLengte = 4;
                schipLetter = "S";
            }
            if (cboVSchip.SelectedIndex == 2)
            {
                schipLengte = 3;
                schipLetter = "K";
            }
            if (cboVSchip.SelectedIndex == 3)
            {
                schipLengte = 3;
                schipLetter = "O";
            }
            if (cboVSchip.SelectedIndex == 4)
            {
                schipLengte = 2;
                schipLetter = "M";
            }

            //booleans om te checken of schip wel mag geplaatst worden!
            bool veldOK = true;
            bool overlapOK = true;


            //eerst clear Zee: check!

            for (int rij = 1; rij <= arrZeePlayer.GetUpperBound(0); rij++)
            {
                for (int kolom = 1; kolom <= arrZeePlayer.GetUpperBound(1); kolom++)
                {
                    if (arrZeePlayer[rij, kolom] == schipLetter)
                    {
                        arrZeePlayer[rij, kolom] = "~";
                    }
                }
            }



            //Ok: Check of schip buiten speelveld komt

            if (vOrien == 1) //Schip horizontaal
            {
                int l = 0;
                while (l < schipLengte)
                {
                    if (vKolom > 10)
                    {
                        veldOK = false;
                        MessageBox.Show("Het schip valt buiten je speelveld, plaats je schip op een andere plaats!");
                        break;
                    }
                    l++;
                    vKolom++;
                }
            }

            else if (vOrien == 2) //schip vertikaal
            {
                int l = 0;
                while (l < schipLengte)
                {
                    if (vRij > 10)
                    {
                        veldOK = false;
                        MessageBox.Show("Het schip valt buiten je speelveld, plaats je schip op een andere plaats!");
                        break;
                    }
                    l++;
                    vRij++;
                }
            }

            else
            {
                MessageBox.Show("Geef alle parameters voor je ship in!");
            }


            //To Do: check voor overlapping met andere schepen
            if (veldOK == true)
            {
                vRij = cboVRij.SelectedIndex + 1;
                vKolom = cboVKolom.SelectedIndex + 1;
                if (vOrien == 1) // schip horizontaal
                {
                    int l = 0;
                    while (l < schipLengte)
                    {
                        if (arrZeePlayer[vRij, vKolom] != "~")
                        {
                            overlapOK = false;
                            MessageBox.Show("Het schip overlapt met een ander schip, plaats je schip op een andere plaats!");
                            break;
                        }
                        l++;
                        vKolom++;
                    }
                }
                else if (vOrien == 2) // schip vertikaal
                {
                    int l = 0;
                    while (l < schipLengte)
                    {
                        if (arrZeePlayer[vRij, vKolom] != "~")
                        {
                            overlapOK = false;
                            MessageBox.Show("Het schip overlapt met een ander schip, plaats je schip op een andere plaats!");
                            break;
                        }
                        l++;
                        vRij++;
                    }
                }
            }



            //Plaats Schip:
            // To do: Enkel uitvoeren als checks ok
            if ((veldOK == true) && (overlapOK == true))
            {
                vRij = cboVRij.SelectedIndex + 1;
                vKolom = cboVKolom.SelectedIndex + 1;
                if (vOrien == 1)
                {
                    int l = 0;
                    while (l < schipLengte)
                    {
                        arrZeePlayer[vRij, vKolom] = schipLetter;
                        l++;
                        vKolom++;
                    }
                }

                if (vOrien == 2)
                {
                    int l = 0;
                    while (l < schipLengte)
                    {
                        arrZeePlayer[vRij, vKolom] = schipLetter;
                        l++;
                        vRij++;
                    }

                }
            }

            ToonZeePlayer();
        }

        private void SchepenPlaatsenComp()
        {
            Random random = new Random();
            bool klaar = false;
            int ditSchip = 0;
            //Done: ditSchip updaten

            while (klaar == false)
            {
                int vRij = random.Next(1, 11);
                int cRij = vRij;
                int vKolom = random.Next(1, 11);
                int cKolom = vKolom;
                int vOrien = random.Next(1, 3);

                int schipLengte = 0;
                string schipLetter = "";

                //parameters schip bepalen
                if (ditSchip == 0)
                {
                    schipLengte = 5;
                    schipLetter = "V";
                }

                if (ditSchip == 1)
                {
                    schipLengte = 4;
                    schipLetter = "S";
                }
                if (ditSchip == 2)
                {
                    schipLengte = 3;
                    schipLetter = "K";
                }
                if (ditSchip == 3)
                {
                    schipLengte = 3;
                    schipLetter = "O";
                }
                if (ditSchip == 4)
                {
                    schipLengte = 2;
                    schipLetter = "M";
                }

                //booleans om te checken of schip wel mag geplaatst worden!
                bool veldOK = true;
                bool overlapOK = true;


                //eerst clear Zee: check!

                for (int rij = 1; rij <= arrOpstellingComputer.GetUpperBound(0); rij++)
                {
                    for (int kolom = 1; kolom <= arrOpstellingComputer.GetUpperBound(1); kolom++)
                    {
                        if (arrOpstellingComputer[rij, kolom] == schipLetter)
                        {
                            arrOpstellingComputer[rij, kolom] = "~";
                        }
                    }
                }



                //Ok: Check of schip buiten speelveld komt

                if (vOrien == 1) //Schip horizontaal
                {
                    int l = 0;
                    while (l < schipLengte)
                    {
                        if (vKolom > 10)
                        {
                            veldOK = false;
                            break;
                        }
                        l++;
                        vKolom++;
                    }
                }

                else if (vOrien == 2) //schip vertikaal
                {
                    int l = 0;
                    while (l < schipLengte)
                    {
                        if (vRij > 10)
                        {
                            veldOK = false;
                            break;
                        }
                        l++;
                        vRij++;
                    }
                }


                //To Do: check voor overlapping met andere schepen
                if (veldOK == true)
                {
                    vRij = cRij;
                    vKolom = cKolom;
                    if (vOrien == 1) // schip horizontaal
                    {
                        int l = 0;
                        while (l < schipLengte)
                        {
                            if (arrOpstellingComputer[vRij, vKolom] != "~")
                            {
                                overlapOK = false;
                                break;
                            }
                            l++;
                            vKolom++;
                        }
                    }
                    else if (vOrien == 2) // schip vertikaal
                    {
                        int l = 0;
                        while (l < schipLengte)
                        {
                            if (arrOpstellingComputer[vRij, vKolom] != "~")
                            {
                                overlapOK = false;
                                break;
                            }
                            l++;
                            vRij++;
                        }
                    }
                }



                //Plaats Schip:
                // To do: Enkel uitvoeren als checks ok
                if ((veldOK == true) && (overlapOK == true))
                {
                    vRij = cRij;
                    vKolom = cKolom;
                    if (vOrien == 1)
                    {
                        int l = 0;
                        while (l < schipLengte)
                        {
                            arrOpstellingComputer[vRij, vKolom] = schipLetter;
                            l++;
                            vKolom++;
                        }
                    }

                    if (vOrien == 2)
                    {
                        int l = 0;
                        while (l < schipLengte)
                        {
                            arrOpstellingComputer[vRij, vKolom] = schipLetter;
                            l++;
                            vRij++;
                        }

                    }
                    ditSchip++;

                    //To Do: check of alle schepen gezet zijn
                    int vSchip = 0;
                    int sSchip = 0;
                    int kSchip = 0;
                    int oSchip = 0;
                    int mSchip = 0;

                    for (int rij = 1; rij <= arrOpstellingComputer.GetUpperBound(0); rij++)
                    {
                        for (int kolom = 1; kolom <= arrOpstellingComputer.GetUpperBound(1); kolom++)
                        {
                            if (arrOpstellingComputer[rij, kolom] == "V")
                            {
                                vSchip++;
                            }
                            if (arrOpstellingComputer[rij, kolom] == "S")
                            {
                                sSchip++;
                            }
                            if (arrOpstellingComputer[rij, kolom] == "K")
                            {
                                kSchip++;
                            }
                            if (arrOpstellingComputer[rij, kolom] == "O")
                            {
                                oSchip++;
                            }
                            if (arrOpstellingComputer[rij, kolom] == "M")
                            {
                                mSchip++;
                            }
                        }

                    }

                    if ((vSchip == 5) && (sSchip == 4) && (kSchip == 3) && (oSchip == 3) && (mSchip == 2))
                    {
                        klaar = true;
                    }
                }
            }
        }

        private void btnKlaar_Click(object sender, EventArgs e)
        {

            //To Do: check of alle schepen geplaatst zijn -> anders MessageBox
            int vSchip = 0;
            int sSchip = 0;
            int kSchip = 0;
            int oSchip = 0;
            int mSchip = 0;
            bool alleSchepenOk = false;
            for (int rij = 1; rij <= arrZeePlayer.GetUpperBound(0); rij++)
            {
                for (int kolom = 1; kolom <= arrZeePlayer.GetUpperBound(1); kolom++)
                {
                    if (arrZeePlayer[rij, kolom] == "V")
                    {
                        vSchip++;
                    }
                    if (arrZeePlayer[rij, kolom] == "S")
                    {
                        sSchip++;
                    }
                    if (arrZeePlayer[rij, kolom] == "K")
                    {
                        kSchip++;
                    }
                    if (arrZeePlayer[rij, kolom] == "O")
                    {
                        oSchip++;
                    }
                    if (arrZeePlayer[rij, kolom] == "M")
                    {
                        mSchip++;
                    }
                }

            }

            if ((vSchip == 5) && (sSchip == 4) && (kSchip == 3) && (oSchip == 3) && (mSchip == 2))
            {
                alleSchepenOk = true;
            }
            else
            {
                MessageBox.Show("Plaats al je schepen!");
            }


            if (alleSchepenOk == true)
            {
                //Done: Plaats schepen vijand! 
                SchepenPlaatsenComp();

                //Done: Hide klaar button
                btnKlaar.Hide();

                //Done: Hide plaats button
                btnVPlaats.Hide();
                cboVKolom.Hide();
                cboVOrient.Hide();
                cboVRij.Hide();
                cboVSchip.Hide();

                //Done: Toon schiet button
                btnSchiet.Show();
                cboSRij.Show();
                cboSKolom.Show();
                btnOpslaan.Show();



                //Toon test opstelling computer
                ToonTestVijand();
            }
        }


        private void btnSchiet_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int sRij = cboSRij.SelectedIndex + 1;
            int sKolom = cboSKolom.SelectedIndex + 1;
            int vRij = 0;
            int vKolom = 0;
            bool plaatsVrij = true;
            bool compVrij = false;

            //Done?: check of hier al geschoten is
            if ((arrZeeComputer[sRij, sKolom] == "N") || (arrZeeComputer[sRij, sKolom] == "G"))
            {
                plaatsVrij = false;
                MessageBox.Show("Hier heb je al eens geschoten. Selecteer een ander vakje!");

            }

            if (plaatsVrij == true)
            {
                //To Do: Messagebox: raak of mis!
                if ((arrOpstellingComputer[sRij, sKolom] == "V") || (arrOpstellingComputer[sRij, sKolom] == "S") || (arrOpstellingComputer[sRij, sKolom] == "K") || (arrOpstellingComputer[sRij, sKolom] == "O") || (arrOpstellingComputer[sRij, sKolom] == "M"))
                {

                    MessageBox.Show("Voltreffer! Vijandig schip geraakt!");
                    arrZeeComputer[sRij, sKolom] = "G";
                    compGeraakt++;

                }
                else
                {
                    MessageBox.Show("Mis! Volgende keer beter!");
                    arrZeeComputer[sRij, sKolom] = "N";
                }

                //Speler gewonnen?
                if (compGeraakt == 17)
                {
                    MessageBox.Show("Proficiat, je hebt gewonnen!");
                    Application.Restart();
                    Environment.Exit(0);
                }


                //To Do: Beurt tegenstander: raak of mis!
                // -> check of hier al geschoten is
                while (compVrij == false)
                {
                    vRij = random.Next(1, 11);
                    vKolom = random.Next(1, 11);
                    if ((arrZeePlayer[vRij, vKolom] != "N") && (arrZeePlayer[vRij, vKolom] != "G"))
                    {
                        compVrij = true;

                    }
                }


                string toonKolom = "";
                switch (vKolom)
                {
                    case 1:
                        toonKolom = "A";
                        break;
                    case 2:
                        toonKolom = "B";
                        break;
                    case 3:
                        toonKolom = "C";
                        break;
                    case 4:
                        toonKolom = "D";
                        break;
                    case 5:
                        toonKolom = "E";
                        break;
                    case 6:
                        toonKolom = "F";
                        break;
                    case 7:
                        toonKolom = "G";
                        break;
                    case 8:
                        toonKolom = "H";
                        break;
                    case 9:
                        toonKolom = "I";
                        break;
                    case 10:
                        toonKolom = "J";
                        break;
                }

                // raak of mis!
                if ((arrZeePlayer[vRij, vKolom] == "V") || (arrZeePlayer[vRij, vKolom] == "S") || (arrZeePlayer[vRij, vKolom] == "K") || (arrZeePlayer[vRij, vKolom] == "O") || (arrZeePlayer[vRij, vKolom] == "M"))
                {

                    MessageBox.Show($"Voltreffer! Je bent geraakt op {vRij} {toonKolom}!");
                    arrZeePlayer[vRij, vKolom] = "G";
                    spelerGeraakt++;

                }
                else
                {
                    MessageBox.Show($"De vijand heeft mis geschoten op {vRij} {toonKolom}!");
                    arrZeePlayer[vRij, vKolom] = "N";
                }

                //Done: Update zee speler en zee vijand
                ToonZeePlayer();
                ToonZeeComp();

                //To Do: Update infobox
                beurten++;
                ToonInfo();

                //To Do: Check of er iemand gewonnen heeft



                //Computer gewonnen?
                if (spelerGeraakt == 17)
                {
                    MessageBox.Show("Helaas, je hebt verloren!");
                    Application.Restart();
                    Environment.Exit(0);
                }



            }
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            Application.Restart();
            Environment.Exit(0);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOpslaan_Click(object sender, EventArgs e)
        {
            string zeePlayer = "";
            string zeeComp = "";
            string zeeCompOpstelling = "";

            //arrays naar strings omzetten
            for (int rij = 1; rij <= arrZeePlayer.GetUpperBound(0); rij++)
            {
                for (int kolom = 1; kolom <= arrZeePlayer.GetUpperBound(1); kolom++)
                {

                    if ((rij == 1) && (kolom == 1))
                    {
                        zeePlayer = arrZeePlayer[1, 1];
                    }
                    else
                    {
                        zeePlayer += "," + arrZeePlayer[rij, kolom];
                    }

                }
            }

            for (int rij = 1; rij <= arrOpstellingComputer.GetUpperBound(0); rij++)
            {
                for (int kolom = 1; kolom <= arrOpstellingComputer.GetUpperBound(1); kolom++)
                {

                    if ((rij == 1) && (kolom == 1))
                    {
                        zeeCompOpstelling = arrOpstellingComputer[1, 1];
                    }
                    else
                    {
                        zeeCompOpstelling += "," + arrOpstellingComputer[rij, kolom];
                    }

                }
            }

            for (int rij = 1; rij <= arrZeeComputer.GetUpperBound(0); rij++)
            {
                for (int kolom = 1; kolom <= arrZeeComputer.GetUpperBound(1); kolom++)
                {

                    if ((rij == 1) && (kolom == 1))
                    {
                        zeeComp = arrZeeComputer[1, 1];
                    }
                    else
                    {
                        zeeComp += "," + arrZeeComputer[rij, kolom];
                    }

                }
            }
            //data wegschrijven naar bestand
            StreamWriter opslaan = new StreamWriter("save.csv");
            opslaan.WriteLine(beurten);
            opslaan.WriteLine(spelerGeraakt);
            opslaan.WriteLine(compGeraakt);
            opslaan.WriteLine(zeePlayer);
            opslaan.WriteLine(zeeCompOpstelling);
            opslaan.WriteLine(zeeComp);
            opslaan.Close();

            //Messagebox
            MessageBox.Show("Spel opgeslagen!");
        }

        private void btnLaden_Click(object sender, EventArgs e)
        {

            if (!(File.Exists("save.csv")))
            {
                MessageBox.Show("Geen opgeslagen spel gevonden!");
            }

            else
            {
                string ladenZeeSpeler = "";
                string ladenOpstComp = "";
                string ladenZeeComp = "";


                //Bestand inlezen
                StreamReader laden = new StreamReader("save.csv");
                beurten = Convert.ToInt32(laden.ReadLine());
                spelerGeraakt = Convert.ToInt32(laden.ReadLine());
                compGeraakt = Convert.ToInt32(laden.ReadLine());
                ladenZeeSpeler = laden.ReadLine();
                ladenOpstComp = laden.ReadLine();
                ladenZeeComp = laden.ReadLine();
                laden.Close();

                //Omzetten naar arrays en game arrays hervullen
                string[] arrLadenZeeSpeler = ladenZeeSpeler.Split(',');
                string[] arrLadenOpstComp = ladenOpstComp.Split(',');
                string[] arrLadenZeeComp = ladenZeeComp.Split(',');

                int teller = 0;
                for (int rij = 1; rij <= arrZeePlayer.GetUpperBound(0); rij++)
                {
                    for (int kolom = 1; kolom <= arrZeePlayer.GetUpperBound(1); kolom++)
                    {
                        arrZeePlayer[rij, kolom] = arrLadenZeeSpeler[teller];
                        teller++;
                    }
                }

                teller = 0;
                for (int rij = 1; rij <= arrOpstellingComputer.GetUpperBound(0); rij++)
                {
                    for (int kolom = 1; kolom <= arrOpstellingComputer.GetUpperBound(1); kolom++)
                    {
                        arrOpstellingComputer[rij, kolom] = arrLadenOpstComp[teller];
                        teller++;
                    }
                }

                teller = 0;
                for (int rij = 1; rij <= arrZeeComputer.GetUpperBound(0); rij++)
                {
                    for (int kolom = 1; kolom <= arrZeeComputer.GetUpperBound(1); kolom++)
                    {
                        arrZeeComputer[rij, kolom] = arrLadenZeeComp[teller];
                        teller++;
                    }
                }


                //schermen hernieuwen
                ToonInfo();
                ToonZeePlayer();
                ToonZeeComp();
                ToonTestVijand();

                //Opstelling knoppen verbergen
                btnKlaar.Hide();
                btnVPlaats.Hide();
                cboVKolom.Hide();
                cboVOrient.Hide();
                cboVRij.Hide();
                cboVSchip.Hide();

                //Knoppen tonen
                btnSchiet.Show();
                cboSRij.Show();
                cboSKolom.Show();
                btnOpslaan.Show();

                //Messagebox
                MessageBox.Show("Spel geladen!");
            }

        }
    }
}
